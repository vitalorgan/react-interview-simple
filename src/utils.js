

const casual = require('casual');




casual.define('user', function() {
    return {
        firstname: casual.first_name,
        lastname: casual.last_name,
        password: casual.password,
        email: casual.email,
        blurb: casual.sentences(3),
        homepage: casual.url,
        favorite_color: casual.rgb_hex,
        address1: casual.address1,
        address2: casual.coin_flip ? casual.address2 : null,
        city: casual.city,
        state: casual.state_abbr,
        zip5: casual.zip(5),
        vacation_pics: [...new Array(casual.integer(0,7)).keys()].map(d =>"https://picsum.photos/600/300")
    };
});


export const generateRandomJson = () => {
    let fakedata = [...new Array(100).keys()].map(d => {
        return casual.user;
    })

    console.log("fake data", fakedata);
    return fakedata;

}
